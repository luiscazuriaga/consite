import { Action, createAction, props, createReducer, on } from '@ngrx/store'

enum ActionTypes {
  GetPatientsData = 'GetPatientsData',
  AddPatientData = 'AddPatientData'
}
// to use with fetch
export const GetPatientsData = createAction(
  ActionTypes.GetPatientsData
)

// to use the redux like a db
export const addPatientData = createAction(
  ActionTypes.AddPatientData,
  props<{ patient: number }>()
)

const INITIAL_STATE = [
  {
    "name": "Rino Alves",
    "subjetiva": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    },
    "objetvia": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    },
    "avaliacao": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    },
    "plano": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    }
  },
  {
    "name": "Monica winter",
    "subjetiva": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    },
    "objetvia": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    },
    "avaliacao": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    },
    "plano": {
      "problema": [
        "tipo1",
        "tipo2"
      ],
      "situação": true,
      "observacao": "texto livre"
    }
  }
]

export const reducer = createReducer(
  INITIAL_STATE,
  on(addPatientData, (state, action: any) => ({ ...state, ...action.patient }))
)
