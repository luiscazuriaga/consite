import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbdTableFiltering } from './table-filtering';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [NgbdTableFiltering],
  exports: [NgbdTableFiltering],
  bootstrap: [NgbdTableFiltering]
})
export class NgbdTableFilteringModule { }
