import { NgModule } from '@angular/core';
import { HomepageComponent } from './homepage.component';
import { CommonModule } from '@angular/common';
import { HomepageRoutingModule } from './homepage-routing.module';
import { LogoComponent } from './logo/logo.component';
import { MenuComponent } from './menu/menu.component';
@NgModule({
  declarations: [HomepageComponent, LogoComponent, MenuComponent,],
  imports: [CommonModule, HomepageRoutingModule],
  exports: [HomepageComponent]
})
export class HomepageModule { }
