import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NgbdTableFilteringModule } from '../table/table-filtering.module';
@NgModule({
  imports: [CommonModule, DashboardRoutingModule, NgbdTableFilteringModule],
  declarations: [DashboardComponent],
  exports: [DashboardComponent]
})
export class DashboardModule { }
