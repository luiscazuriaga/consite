import { Component, OnInit } from '@angular/core';
import { Store, select, State } from '@ngrx/store'
import { Observable } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  count$: Observable<number>;

  constructor() {
  }


  ngOnInit(): void{}


}
