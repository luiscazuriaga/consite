import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnchiridionComponent } from './enchiridion.component';

describe('EnchiridionComponent', () => {
  let component: EnchiridionComponent;
  let fixture: ComponentFixture<EnchiridionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnchiridionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnchiridionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
