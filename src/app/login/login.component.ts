import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  activeInput1 = false
  activeInput2 = false
  value = ""

  InputChange1Focus(e) {
    this.activeInput1 = true
  }

  InputChange1Blur(e) {
    if (e.target.value === "")
      this.activeInput1 = false
  }

  InputChange2Focus(e) {
    this.activeInput2 = true
  }

  InputChange2Blur(e) {
    if (e.target.value === "") {
      this.activeInput2 = false
    }
  }

}
