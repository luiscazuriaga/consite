import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/homepage' },
  { path: 'login', pathMatch: 'full', redirectTo: '/login' },
  { path: 'dashboard', pathMatch: 'full', redirectTo: '/dashboard' },
  { path: 'enchiridion', pathMatch: 'full', redirectTo: '/enchiridion' },
  { path: '**', pathMatch: 'full', redirectTo: '/homepage' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
