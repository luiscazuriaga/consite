import { NgModule } from '@angular/core';
import { EnchiridionComponent } from './enchiridion.component';
import { CommonModule } from '@angular/common';
import { EnchiridionRoutingModule } from './enchiridion-routing.module';

@NgModule({
  imports: [CommonModule, EnchiridionRoutingModule],
  declarations: [EnchiridionComponent],
  exports: [EnchiridionComponent]
})
export class EnchiridionModule { }
