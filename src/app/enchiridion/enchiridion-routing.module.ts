import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnchiridionComponent } from './enchiridion.component';

const routes: Routes = [
  { path: 'enchiridion', component: EnchiridionComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnchiridionRoutingModule { }
